const PDF_DOWNLOAD_URL =
  "http://invoices.prd.vcs.cimpress.io/v1/sap-download-invoice";
// STG ENV
/* const clientId = "348WSgkvPE6Cqmjc2ctf3QUmkDOAiCSU";
const clientSecret =
  ".3dDFcDeBuBnC4DrCeHNDGC2xBcq2DWCADDV6DpZB9CNbgDAqBJDJDQBGuB2CNzD";
const BASE_URL_SUBSCRIPTION_SVC =
  "https://subscription-service-staging.orders.vpsvc.com"; */

let token = "";
const getAccessToken = async () => {
  if (token) {
    return token;
  }
  let response = await fetch("https://oauth.cimpress.io/v2/token", {
    method: "POST",
    body: JSON.stringify({
      client_id: clientId,
      client_secret: clientSecret,
      audience: "https://api.cimpress.io/",
      grant_type: "client_credentials",
    }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Host: "https://invoices.prd.vcs.cimpress.io/",
      Referer: "https://invoices.prd.vcs.cimpress.io/",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST",
      "Access-Control-Allow-Headers":
        "Content-Type, Authorization, Content-Length, X-Requested-With, Accept",
      "User-Agent": "My app",
    },
  });
  response = await response.json();
  token = response.access_token;
  return token;
};

const getPDFData = async (data) => {
  const url = `${PDF_DOWNLOAD_URL}`;

  return fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: new Headers({
      Accept: "application/json",
      "Content-Type": "application/json",
      Host: "https://invoices.prd.vcs.cimpress.io/",
      Referer: "https://invoices.prd.vcs.cimpress.io/",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST",
      "Access-Control-Allow-Headers":
        "Content-Type, Authorization, Content-Length, X-Requested-With, Accept",
      "User-Agent": "My app",
    }),
  })
    .then((resp) => resp.json())
    .then((data2) => {
      // console.log("getPDFData", data2);
      downloadBase64File(data2.PdfInvoiceData, data2.PdfInvoiceFileName);
    });
};

function downloadBase64File(contentBase64, fileName) {
  const linkSource = `data:application/pdf;base64,${contentBase64}`;
  const downloadLink = document.createElement("a");
  document.body.appendChild(downloadLink);

  downloadLink.href = linkSource;
  downloadLink.target = "_self";
  downloadLink.download = fileName;
  downloadLink.click();
}

function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(",")[1]);

  // separate out the mime component
  var mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  var ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], { type: mimeString });
  return blob;
}

$(document).ready(function () {
  /*  getPDFData({
    PartnerId: "DesignCrowd-621b9abf-8a67-4eae-a5bf-7f3c22324c3b",
    InvoiceIds: ["0215754276", "0215754282", "0215753871", "0215754284"],
  }); */
  // var blob = new Blob(pdfResp.PdfInvoiceData);
  // var myBlob = pdfResp.PdfInvoiceData;

  /* var blobUrl = URL.createObjectURL(dataURItoBlob(pdfResp.PdfInvoiceData));

  var link = document.createElement("a"); // Or maybe get it from the current document
  link.href = blobUrl;
  link.download = pdfResp.PdfInvoiceFileName;
  link.innerText = "Click here to download the file";
  document.body.appendChild(link); // Or append it whereever you want */

  $("#btn-download").click(function () {
    var resp = $("#input-resp").val();
    var respObj = JSON.parse(resp);
    downloadBase64File(respObj.PdfInvoiceData, respObj.PdfInvoiceFileName);

    /* var partnerId = $("#input-partnerid").val();
    var invoiceIds = $("#input-invoiceids").val();

    getPDFData({
      PartnerId: "DesignCrowd-621b9abf-8a67-4eae-a5bf-7f3c22324c3b",
      InvoiceIds: [215754276, 215754282, 215753871, 215754284],
    }); */

  });

  // downloadBase64File(pdfResp.PdfInvoiceData, pdfResp.PdfInvoiceFileName);
});
